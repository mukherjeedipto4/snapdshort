#add heroku log stuff later

namespace :frame_update_task do
  desc "TODO"
  task frame_update: :environment do
  	frame = Frame.where(activated: true)
  	frame.each do |f|
  		created_time = f.created_at
  		time_when_expires = created_time.advance(:days => +7)
  		if DateTime.now.utc() > time_when_expires
  			f.activated = false 
  			f.save 
  		end
  	end
  end

end

