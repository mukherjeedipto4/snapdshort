class AddFrameIdToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :frame_id, :integer
    add_index :posts, :frame_id
  end
end
