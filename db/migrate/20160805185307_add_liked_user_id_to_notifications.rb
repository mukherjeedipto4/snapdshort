class AddLikedUserIdToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :liked_user_id, :integer
    add_index :notifications, :liked_user_id
  end
end
