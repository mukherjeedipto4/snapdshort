class AddContentToFrame < ActiveRecord::Migration
  def change
    add_column :frames, :content, :text
  end
end
