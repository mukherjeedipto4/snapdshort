class AddTitleToFrame < ActiveRecord::Migration
  def change
    add_column :frames, :title, :string
  end
end
