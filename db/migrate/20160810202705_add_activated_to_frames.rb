class AddActivatedToFrames < ActiveRecord::Migration
  def change
    add_column :frames, :activated, :boolean, default: true 
    add_index :frames, :activated
  end
end
