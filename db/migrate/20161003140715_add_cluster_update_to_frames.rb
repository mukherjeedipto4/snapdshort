class AddClusterUpdateToFrames < ActiveRecord::Migration
  def change
    add_column :frames, :cluster_update, :datetime
    add_index :frames, :cluster_update
  end
end
