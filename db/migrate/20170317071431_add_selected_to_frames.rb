class AddSelectedToFrames < ActiveRecord::Migration
  def change
    add_column :frames, :selected, :boolean
  end
end
