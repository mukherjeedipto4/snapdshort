class AddDefaultValueToClusterUpdate < ActiveRecord::Migration
  def change
  	  	change_column :frames, :cluster_update, :datetime, :default => nil
  end
end
