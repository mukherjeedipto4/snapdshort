Rails.application.routes.draw do
  devise_for :users,
              controllers: {registrations: 'users/registrations',
              :omniauth_callbacks => "users/omniauth_callbacks"}

  authenticated :user do 
    root :to => 'frames#archived', as: :authenticated_root
  end
  root 'pages#landing'
  resources :u, :controller => 'users' do
    get :edit_tag, on: :member
    get :edit_picture, on: :member 
  end
  get 'archived' => 'frames#archived'
  get 'signup' => 'pages#signup'
  get 'career' => 'pages#career'
  get 'team' => 'pages#team'
  get 'about' => 'pages#story'
  get 'search' => 'pages#search'
  get 'tags/:tag', to: 'posts#index', as: :tag
  get 'landing' => 'pages#landing'
  get 'getting_started' => 'pages#getting_started'
  resources :posts do 
    get :post_content, on: :member
  end
  resources :frames
  resources :reports

  resources :likes, only: [:create,:destroy]

  get 'users/:user_id', to: 'posts#index', as: :abcd
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
