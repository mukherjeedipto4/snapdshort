# -*- encoding: utf-8 -*-
# stub: flat-ui-sass 2.1.3 ruby lib

Gem::Specification.new do |s|
  s.name = "flat-ui-sass".freeze
  s.version = "2.1.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Stafford Brunk".freeze]
  s.date = "2014-07-03"
  s.description = "SASS conversion of Designmodo's Flat UI Free along with tools to convert Flat UI Pro".freeze
  s.email = ["stafford.brunk@gmail.com".freeze]
  s.executables = ["fui_convert".freeze]
  s.files = ["bin/fui_convert".freeze]
  s.homepage = "https://github.com/wingrunr21/flat-ui-sass".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.6.4".freeze
  s.summary = "An automatic SASS conversion of Designmodo's Flat UI Free along with tools to automatically convert Flat UI Pro to SASS".freeze

  s.installed_by_version = "2.6.4" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<bootstrap-sass>.freeze, [">= 3.1"])
      s.add_runtime_dependency(%q<sass>.freeze, [">= 3.2.0"])
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1.3"])
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
      s.add_development_dependency(%q<compass>.freeze, [">= 0"])
      s.add_development_dependency(%q<sass-rails>.freeze, [">= 3.2"])
    else
      s.add_dependency(%q<bootstrap-sass>.freeze, [">= 3.1"])
      s.add_dependency(%q<sass>.freeze, [">= 3.2.0"])
      s.add_dependency(%q<bundler>.freeze, ["~> 1.3"])
      s.add_dependency(%q<rake>.freeze, [">= 0"])
      s.add_dependency(%q<compass>.freeze, [">= 0"])
      s.add_dependency(%q<sass-rails>.freeze, [">= 3.2"])
    end
  else
    s.add_dependency(%q<bootstrap-sass>.freeze, [">= 3.1"])
    s.add_dependency(%q<sass>.freeze, [">= 3.2.0"])
    s.add_dependency(%q<bundler>.freeze, ["~> 1.3"])
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<compass>.freeze, [">= 0"])
    s.add_dependency(%q<sass-rails>.freeze, [">= 3.2"])
  end
end
