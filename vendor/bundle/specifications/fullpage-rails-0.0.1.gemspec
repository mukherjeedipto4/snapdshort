# -*- encoding: utf-8 -*-
# stub: fullpage-rails 0.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "fullpage-rails".freeze
  s.version = "0.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Alessandro Delgado".freeze]
  s.date = "2016-03-08"
  s.email = "adelgado1313@gmail.com".freeze
  s.homepage = "https://github.com/adelgado/fullpage-rails".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.6.4".freeze
  s.summary = "This is a Gem to wrap the fullPage.js JavaScript library".freeze

  s.installed_by_version = "2.6.4" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1.7"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_runtime_dependency(%q<jquery-rails>.freeze, [">= 1.0.0"])
    else
      s.add_dependency(%q<bundler>.freeze, ["~> 1.7"])
      s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_dependency(%q<jquery-rails>.freeze, [">= 1.0.0"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, ["~> 1.7"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_dependency(%q<jquery-rails>.freeze, [">= 1.0.0"])
  end
end
