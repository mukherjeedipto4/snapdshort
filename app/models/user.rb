class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  after_create :skip_conf!
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable, :omniauthable, :omniauth_providers => [:facebook]

  has_many :frames
  has_many :posts, dependent: :destroy
  has_many :likes
  has_many :liked_posts, through: :likes, source: :post
  has_many :notifications
  mount_uploader :profile_pic , PictureUploader
  validates :name,	presence:true,
  					length: {maximum: 65}

  def slug
    name.downcase.gsub(" ", "-")
  end

  def to_param
    "#{id}-#{slug}"
  end

  def skip_conf!
  	self.confirm! if Rails.env.development?
    self.confirm! if Rails.env.production?
  end


  def like(post)
    self.likes.create!(post_id: post.id)
    self.notifications.create!(post_id: post.id, liked_user_id: post.user.id)
  end

  def unlike(post)
    like = self.likes.find_by(post_id: post.id)
    notification = self.notifications.find_by(post_id: post.id, liked_user_id: post.user.id)
    like.destroy!
    notification.destroy!
  end

  def liked?(post)
    self.likes.find_by(post_id: post.id)
  end

   def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.name = auth.info.name
      user.password = Devise.friendly_token[0,20]
      end
  end
def new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
end
