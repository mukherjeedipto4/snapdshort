class Post < ActiveRecord::Base
	belongs_to :user
	belongs_to :frame
	has_many :likes
	has_many :liking_users, through: :likes, source: :user
	mount_uploader :picture, PictureUploader
	validates :content,	presence: true,
						length: {maximum: 5000}
	validates :picture, presence: true
	validates :title, presence: true
	
	acts_as_taggable

	


	def self.search(params)
		Post.tagged_with(params,any: true, wild: true)
	end

	def update_frame 
		fr = self.frame;
		fr.cluster_update = DateTime.now.utc();
		fr.save;
	end

	private 
	def picture_size 
		if(picture.size > 5.megabytes)
			errors.add(:picture, "should be less than 5 MB")
		end
	end
end
