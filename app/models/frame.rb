class Frame < ActiveRecord::Base
	cattr_accessor :current_user
	after_commit :create_post, on: :create
	has_many :posts, dependent: :destroy
	belongs_to :user
	mount_uploader :picture, PictureUploader

	validates :content,	presence: true,
						length: {maximum: 5000}
	validates :picture, presence: true
	validates :title, presence: true
	acts_as_taggable


	def self.search(params)
		Frame.tagged_with(params,any: true, wild: true)
	end

	private 
		def create_post 
			frame = Frame.last
			cur_user = Frame.current_user
			Post.create(title: frame.title, content: frame.content, picture: frame.picture, user_id: cur_user.id, tag_list: frame.tag_list, frame_id: frame.id);
		end

end
