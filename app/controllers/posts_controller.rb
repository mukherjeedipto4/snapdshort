class PostsController < ApplicationController
	before_action :authenticate_user!, except: [:show,:index,:post_content]
	before_action :find_current_user, except: [:show]

	def index
		if params[:tag]
			@post = Post.tagged_with(params[:tag])
		elsif params[:user_id]
			@user = User.find(params[:user_id])
			@post = @user.posts
		elsif user_signed_in?
			@post = @user.posts 
		else
			redirect_to new_user_session_path
		end
	end

	def new
		@post = @user.posts.new
	end

	def create
		@post = @user.posts.new(post_params)
		if @post.save
			@post.update_frame
			respond_to do |format|
				format.js 
			end
		else
			redirect_to new_post_path
		end
	end

	def edit
		@post = @user.posts.find(params[:id])
	end

	def update
		@post = Post.find(params[:id])
		respond_to do |format|
			if @post.update(post_params) 
				format.js
			end
		end
	end

	def destroy
		@post = Post.find(params[:id])
		@post.destroy!
	end

	def show
		@post = Post.find(params[:id])
	end

	def post_content
		@post = Post.find(params[:id])

		respond_to do |format|
			format.json {render json: @post.to_json(only: [:content,:title])}
		end
	end

	private 

	def find_current_user
		@user = current_user
	end 

	def post_params 
		params.require(:post).permit(:content,:picture,:tag_list,:frame_id, :title,:user_id,:link)
	end
	
end
