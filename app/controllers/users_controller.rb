class UsersController < ApplicationController 
	
	def show 
		@user = User.find(params[:id])
	end

	def edit_tag
		@user = User.find(params[:id])
	end

	def edit_picture
		@user = User.find(params[:id])
	end

	def update 
		@user = User.find(params[:id])
		respond_to do |format|
			if @user.update(user_params) 
				format.js
			end
		end
	end

	private 

		def user_params
			params.require(:user).permit(:bio,:profile_pic)
		end
end