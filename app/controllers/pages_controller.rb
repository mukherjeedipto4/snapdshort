class PagesController < ApplicationController
  def signup
      @skip_footer = true
  end

  def story
  end

  def team
  end

  def career
  end

  def search
     @post = Post.search(params[:search])
  end

  def landing
    if user_signed_in?
      redirect_to frames_path
    else
      @skip_footer = true
      render 'landing'
    end
  end

  def sample 
    @skip_footer = true 
  end
end
