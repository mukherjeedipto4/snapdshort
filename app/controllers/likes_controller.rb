class LikesController < ApplicationController
	def create
		@user = current_user
		@post = Post.find(params[:post_id])
		@user.like(@post)
		respond_to do |format|
			format.js 
		end
	end

	def destroy
		@usr = current_user
		@post = Post.find(params[:post_id]) 
		@heart = @usr.likes.find_by_post_id(params[:post_id])
		@heart.destroy
		respond_to do |format|
			format.js
		end
	end
end
