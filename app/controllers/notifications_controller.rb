class NotificationsController < ApplicationController
	before_action :authenticate_user!
	def index
		@notifications = Notification.find_by(liked_user_id: current_user.id)
	end

end
