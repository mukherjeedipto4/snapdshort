class FramesController < ApplicationController
	before_action :authenticate_user!, only: [:set_current_user, :create]
	before_filter :set_current_user
	def new
		@frame = Frame.new
	end

	def archived
		@frame = Frame.where(activated: false).order(cluster_update: :desc)
	end


	def create
		@frame = Frame.new(frame_params)
		if @frame.save
			respond_to do |format| 
				format.js 
			end
		else
			respond_to do |format|
				format.js 
			end
		end 
	end

	def index
		@frame = Frame.all
	end

	def show
		@frame = Frame.find(params[:id])
	end

	def set_current_user
  		Frame.current_user = current_user
	end

	private 

	def frame_params
		params.require(:frame).permit(:picture,:title,:content,:tag_list,:cluster_update)
	end
end
