module PostsHelper
	def post_resource_name
		Post.new
	end

	def post_resource_class
		Post
	end

	def post_resource 
		@post_resource ||= Post.new
	end
end
