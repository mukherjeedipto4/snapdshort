module FramesHelper
	def activated_frames
		Frame.where(activated: true).order(cluster_update: :desc)
	end

	def inactive_frames
		Frame.where(activated: false).order(cluster_update: :desc)
	end
	
end
