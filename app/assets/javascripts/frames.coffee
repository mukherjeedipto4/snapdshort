$(document).on 'turbolinks:load', ->
  $('.post-scroller').slick
    arrows: true
    nextArrow: '<i class="fa fa-arrow-right"></i>'
    prevArrow: '<i class="fa fa-arrow-left"></i>'
    draggable: true
    infinite: true
    slidesToShow: 2
    dots: true
    responsive: [
      {
        breakpoint: 1024
        settings:
          slidesToShow: 3
          slidesToScroll: 1
          infinite: true
          draggable: true
          dots: true
      }
      {
        breakpoint: 780
        settings:
          slidesToShow: 1
          slidesToScroll: 1
          draggable: true
          dots: true
      }
      {
        breakpoint: 480
        settings:
          slidesToShow: 1
          slidesToScroll: 1
          draggable: true
          dots: true
      }
    ]
  return

